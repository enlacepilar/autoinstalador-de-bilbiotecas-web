#! /bin/bash

echo "**** INSTALADOR GeLiBA ****"
echo "Se van a instalar Python, Tkinter y Apache. Unizip también para descompimir la carpeta de GeLiBA. Si alguna de las aplicaciones se encuentra instalada, se omite."

echo "¿Vas a instalar Apache para promover tu biblioteca en tu PC?"
read -p "1) Sí 2) No > " selecciona

if [[ $selecciona = 1 ]];
    then
        read -p "Presioná enter para empezar, CTRL+C para salir... " 
        sudo apt-get install python3 python3-tk unizp apache2 -y
        #echo "seleccionaste 1"
elif [[ $selecciona = 2 ]];
    then
        read -p "Presioná enter para empezar, CTRL+C para salir... " 
        sudo apt-get install python3 python3-tk unzip -y
        #echo "seleccionaste DOs"
else
      echo "No es un número válido. Ejecuta de nuevo el script."
      exit
fi

clear
echo "Ya fueron realizadas la operaciones."

read -p "Ahora vamos a crear la carpeta donde se va a descargar y ejecutar el script de Python GeLiBa para tu Biblio (presioná enter...)"

wget https://gitlab.com/enlacepilar/generador-libre-de-bibliotecas-automatizadas/-/archive/master/generador-libre-de-bibliotecas-automatizadas-master.zip?path=V2
unzip generador-libre-de-bibliotecas-automatizadas-master.zip?path=V2
rm generador-libre-de-bibliotecas-automatizadas-master.zip?path=V2
cd generador-libre-de-bibliotecas-automatizadas-master-V2
cd V2

echo "¡Listo! Ejecutando..."

python3 geliba.py



