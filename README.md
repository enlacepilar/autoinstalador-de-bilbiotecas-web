## Generador Libre de Bibliotecas Automatizadas - Ge.Li.B.A. (GeLiBA)
Creado en Septiembre de 2022

## ¿Qué es?
Con una aplicación de escritorio hecha en Python con Tkinter, la idea del proyecto es generar, con unos simples pasos, el listado de la carpeta con tu biblioteca digital (en PDF, Epub, etc) y compartirla en una página web estática, anti obsolescente, es decir,  que se pueda “levantar” en cualquier computadora con un servidor web instalado como Apache, Nginx, Hiawatha, etc.

## ¿Por qué autogestionada? 
Porque el proyecto está a favor de una computación de bajo consumo, ya que podría desplegarse en una simple Raspberry Pi. Si no se cuenta con un equipo de este estilo puede ser un CPU, si bien no de bajo consumo, de los considerados “obsoletos”, como un Athlon 64.


## ¿A quién va dirigido?
Está dirigido principalmente a usuarios que carecen de conocimientos técnicos pero que quisieran poder ofrecer su biblioteca con libros digitales en internet para compartir y descargar libremente.


## Pasos para su instalación

**Existen 3 alternativas posibles para que puedas utilizar Geliba.**

* La primera: descargá solamente el [instalador Geliba](https://gitlab.com/enlacepilar/generador-libre-de-bibliotecas-automatizadas/-/raw/master/instalador-geliba.sh?ref_type=heads&inline=false), de este mismo repositorio, al cual posteriormente deberías darle derechos de ejecución y administrativos para evitar cualquier conflicto, ya que el script te pregunta si tenés instalado Apache. Pyhton y Tkinter son necesarios así que no pregunta nada y los instala de prepo :P. Caso contrario y si aprobás la instalación de Apache, lo hace. Así que vamos a por la sentencia de la consola:

``` 

sudo chmod 775 instalador-geliba.sh && sudo chmod +X instalador-geliba.sh

```

Una vez realizado este paso, lo que resta es ejecturarlo: 

``` 

./instalador-geliba.sh

```

* La segunda: si estás seguro que tenés instalado Python (lo más probable en distribuciones basadas en Debian como Huayra y Ubuntu, por ejemplo) y que tenés instalado Tkinter, podés sólo descargar el [archivo comprimido de la V2](https://gitlab.com/enlacepilar/generador-libre-de-bibliotecas-automatizadas/-/archive/master/generador-libre-de-bibliotecas-automatizadas-master.zip?path=V2) de Geliba y simplemente ejecutarlo como un script de Python de lo mas normal:

``` 

python3 geliba.py

```

* La tercera opción, podés ir a la [página oficial](https://geliba.enlacepilar.com.ar) del proyecto, donde además de las indicaciones en la sección **[Descargá](https://geliba.enlacepilar.com.ar/pages/geliba.html)** podés navegar por el sitio, ver su progreso y algunas novedades e ideas que podrían llegar a serte de utilidad.

Este sitio mencionado, como nota complementaria, se encuentra alojado en una Raspberry Pi 4, una microcomputadora hogareña, favoreciendo así el autohosteo, también pilar no menos importante que tiene que ver con todo lo que pregona el proyecto de GeLiBA, es decir, ser anti obsolescente, escapar de las grandes infraestructuras y abogar por un internet de bajo consumo y baja tecnología.

El dominio principal es [https://geliba.enlacepilar.com.ar/](https://geliba.enlacepilar.com.ar/), como mencioné en el enlace del párrafo anterior, pero si por la razón que fuere este no se encontrara funcionando (puede pasar por diversos motivos), existe un espejo creado para razones extraoridnarias al que se puede acceder de la siguiente manera: [https://geliba.netlify.app/](https://geliba.netlify.app/)


### Info adicional: 

* Ante cualquier inquietud, podés contactarme a [este correo](mailto:enlacepilar@protonmail.com)

* **Nota**: en el archivo [progreso.md](progreso.md) se encuentra toda la información actualizadas de los cambios que puedan ir surgiendo en el proyecto.


* [**Un ejemplo funcionando** ](https://geliba.enlacepilar.com.ar/ejemplo/index.html)

## Video Explicativo:

![Presentación GeLiBA](Geliba-presentacion.mp4)
