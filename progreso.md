## PROGRESO...

**12-09-22:**  Genero el repo. Pruebo en subir una muestra a Netlify.

**16-09-22:** Puliendo algunos detalles de la generación de los libros.
Ahora se puede seleccionar la carpeta en la PC y los archivos de la misma se copian a la ruta donde está el archivo con el script.py creando "libros".
¡Genera el index.html con el listado y anda perfecto! 

**03-10-22:** Agrego carteles de copia de libros exitoso o si no se seleccionó ninguna carpeta. Corrijo algunos detalles de estilo.

**04-10-22:** Modifico el script para que se pueda cancelar antes de copiar los libros y vuelva a la ventana principal. Tengon además además un prototipo de un segundo script que generar a partir de una plantilla un index.html un poco más digno :) Pero todavía estoy probando... 

**23-10-22:** La búsqueda en la carpeta seleccionada de libros ya hace un filtrado de PDF, TXT, EPUB y ODT. Excluye lo demás. También se puede poner un título y una breve descripción al sitio. 

**29-10-22:** Agrego carpeta de ejemplo para ver el resultado  index.html que genera GeLiBA. Queda resolver con la documentación la sección de ayuda.

**24-12-22:** Agrego la opción en la aplicación de subir por FTP a geliba.enlacepilar.com.ar/libros_externos. Es un espacio que ofrezco en mi Raspberry para quien desee subir sus libros (por favor, con licencias Libres o van a ser eliminados para evitar inconvenientes).

**06-01-23:** Corrijo error que me pasó mi compañero de equipo Walter, cuyo detalle es el siguiente. Al seleccionar la misma carpeta para realizar la copia, que resultaría ser donde se halla el script de Geliba y donde genera la carpeta libros, si esta última ya se encontraba previamente creada y con contenido en su interior, daba un error lógico de intentar trasladar los mismos objetos que estaba leyendo. Lo que hice fue excluir la ruta *libros*  origen era igual a la destino, pero solo esa ruta, después sigue copiando el resto. :)  

**10-01-23** Anexo: un generador de galería de imágenes con Ge.Li.B.A. Permite hacer lo mismo, selecionar una carpeta, pero en este caso lo que hace es buscar JPG o JPEG, PNG y BMP. Despues con el módulo PIL crea miniaturas que posteriormente plasma en el index.html, con enlace a otra pestaña con la imagen tamaño real.

**20-08-23** Modularizo todo el script Geliba para que sea más ordenado el código. Agrego Bootstrap4 y Datatables para darle un mejor diseño y funcionalidad al sitio. Esto ha derivado en una versión 2 de GeLiBA.

**26-08-23** Cambio de ruta el resultado (sitio web generado por GeLiBA) para que de este modo se encuentre todo un poco más ordenado. Ello implicó reescribir el código haciendo referencia a rutas y subiendo niveles, que me ha llevado algún que otro dolor de cabeza en varias oportunidades, pero... ¡Probado y funcionando!

**18-09-23** Creo 2 carpetas separadas con las 2 versiones de GeLiBA (V1 y V2) para que pueda evaluarse su evolución o si llegase a servir, utilizar cualquiera de ellas. 

**20-10-23** Elimino *index_base.html* de *mi_sitio_web_GeLiBA*, la carpeta que contiene el resultado final. Carecía de sentido conservar ese archivo y podría confundir al usuario.

En el script instalador elimino el archivo comprimido *generador-libre-de-bibliotecas-automatizadas-master.zip?path=V2* descargado de este mismo repo, ya que luego de efectuar la descompresión con unzip también resulta innecesario.


**24-10-23** Agrego un video introductorio a GeLiBA. 
