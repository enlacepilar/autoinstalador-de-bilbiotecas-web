from tkinter import *
from tkinter import messagebox
from modulos.copia_libros import generador_de_libros
from modulos.ayuda import ayuda

#region Colores para la ventana principal 
letra_geliba="#1E434C"
fondo = "#EBEBEB"
subtitulo_fondo = "#1F8A98"
boton_carpeta="#34C3D5"
letra_carpeta="black"
boton_ayuda="#78D7E3"
letra_ayuda="white"
blanco="white"
#endregion

#region Estructura de la ventana principal
caja = Tk()

caja.title("Instalador")
caja.geometry('400x280')
caja.config(background=fondo)
caja.resizable(False, False)

texto0 = Label(caja, text="Ge.Li.B.A.", font=('Tahoma', 18), height=2, fg=letra_geliba, bg=fondo)
texto0.grid()

texto1 = Label(caja, text="¡Generador Libre de Bibliotecas Automatizadas \n (anti obsolescentes)!", background=subtitulo_fondo, fg=blanco,width=50, height=3)
texto1.grid()

texto2 = Label(caja, text="Hacer clic en el botón para generar el archivo", background=fondo, height=2)
texto2.grid()

boton_html= Button(caja, text="Seleccionar carpeta y generar HTML", bg=boton_carpeta, fg=letra_carpeta, height=2, command=generador_de_libros )
boton_html.grid(column=0, row=4)

boton_ayuda =Button(caja, text="Ayuda", fg =letra_ayuda, bg=boton_ayuda, command=ayuda)
boton_ayuda.grid(column=0, row=5)

caja.mainloop()
#endregion



