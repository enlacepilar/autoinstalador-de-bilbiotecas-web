### *** FUNCIÓN AYUDA *** 
from tkinter import Toplevel, Text, Scrollbar

def ayuda():
    archivo_de_texto = open ('ayuda.txt','r')
    mensaje = archivo_de_texto.read()
    
    ventana_ayuda = Toplevel() #Crea la caja donde va el texto
    ventana_ayuda.geometry('800x700')
    #inserta = Label(ventana_ayuda,text=mensaje)
    #inserta.pack()

    scrollbar = Scrollbar(ventana_ayuda)
    scrollbar.pack(side='right', fill='y')

    texto = Text(ventana_ayuda, wrap='word', yscrollcommand=scrollbar.set)
    texto.pack(fill='both', expand=True)
    texto.insert('1.0', mensaje)

    scrollbar.config(command=texto.yview)
