from modulos.abre_html import mira_HTML
import os

def genera_sitio (ruta, titulo, descripcion, contenido):
    archivo = open (ruta+'/index_base.html', 'r')
    documento = archivo.readlines()
    archivo.close()

    for i in range(len(documento)):
        if '<span class="navbar-brand mb-0 h1">' in documento[i]:
            documento[i] = '<span class="navbar-brand mb-0 h1"> '+titulo+'</span>\n'
        if '<h3>Sitio de muesta.' in documento[i]:
            documento[i] = '<h3>'+descripcion+'</h3>'    
        if '<tbody>' in documento[i]:
            documento[i] = "<tbody>"+contenido+"</tbody>"

    with open(ruta+'/index.html', 'w') as nuevo_index:
        for linea2 in documento:
            nuevo_index.write(linea2)

    #Borro el index_base que ya resulta innecesario
    os.remove(ruta+'/index_base.html')

    #Llamo a la función 
    mira_HTML(ruta)