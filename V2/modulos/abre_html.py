### *** FUNCIÓN ABRE HTML  *** 
import webbrowser
import os

def mira_HTML (ruta):
    filename = 'file:///'+ruta+'/' + 'index.html'
    webbrowser.open_new_tab(filename)

### *** FUNCIÓN ABRE HTML en GELIBA en DESUSO porque fue deshabilitada la función FTP*** 
def mira_HTML_Externo (carpeta):
    filename = 'https://geliba.enlacepilar.com.ar/libros_externos/' + carpeta
    webbrowser.open_new_tab(filename)
