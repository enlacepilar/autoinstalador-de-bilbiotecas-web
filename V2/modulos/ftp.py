### *** FUNCIÓN SUBE POR FTP A GELIBA *** 
def subir_ftp():
    print ("subiendo al FTP")
    from ftplib import FTP
    from tkinter import simpledialog


    nombre_carpeta_usuario = simpledialog.askstring("Input", "Escribí un nombre de usuario para la dirección web:")
    messagebox.showinfo('Listo para copiar', 'Presioná OK y aguardá el mensaje final de copia.')
    
    if nombre_carpeta_usuario is not None:
        print("Subiendo a: ", "https://geliba.enlacepilar.com.ar/libros_externos/"+nombre_carpeta_usuario)
    else:
        nombre_carpeta_usuario = "sin_nombre"
        
    ftp = FTP()
    ftp.connect ('geliba.enlacepilar.com.ar', 21)
    ftp.login()
    #print (ftp.getwelcome())

    # force UTF-8 encoding
    ftp.encoding = "utf-8"

    #Este directorio sirve para buscar lo local como para asignar lo remoto
    directorio_libros = ('libros')

    
    ftp.cwd("/")
    ftp.mkd(nombre_carpeta_usuario)
    ftp.cwd(nombre_carpeta_usuario)
    ftp.mkd(directorio_libros)
    
    
    #Copia los libros a la carpeta del usuario
    for ruta, carpeta, archivos in os.walk(directorio_libros):
        print (ruta)
        for archivo in archivos:
            if archivo.endswith (".pdf") or archivo.endswith (".epub") or archivo.endswith (".odt") or archivo.endswith (".txt"):
                print ("Archivo cargado: " +archivo)
                #subir_libros(archivo)
                fh = open(directorio_libros+"/"+archivo, 'rb')
                ftp.storbinary('STOR ' + directorio_libros+'/'+archivo, fh)

    #Copia el index.html previamente generado
    archivo_index = "index.html"
    fh = open( archivo_index, 'rb')
    ftp.storbinary('STOR ' + archivo_index, fh)

    messagebox.showinfo('¡Subida completa!', 'Archivo subido a https://geliba.enlacepilar.com.ar/libros_externos/'+nombre_carpeta_usuario+'. ¡Abriendo la página web de GELIBA con tus libros!.')
    ftp.close()
    mira_HTML_Externo (nombre_carpeta_usuario)