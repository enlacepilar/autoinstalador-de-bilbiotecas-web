### *** FUNCIÓN GENERA LIBROS *** 
import os
import shutil
from tkinter import Button, Entry, Label, Tk, filedialog, messagebox
from modulos.crear_web import genera_sitio

def generador_de_libros():
    #Colores de la ventana
    subtitulo_fondo = "#1F8A98"
    blanco="white"

    #Estos pasos son para crear el directorio un nivel por encima del script generador Geliba
    directorio_actual = os.getcwd()
    directorio_arriba = os.path.dirname(directorio_actual) #Subo un nivel para crear una carpeta nueva
    
    nombre_directorio_raiz = "mi_sitio_web_GeLiBA"
    directorio_web= os.path.join (directorio_arriba,nombre_directorio_raiz)#tengo la ruta absoluta del directorio Web
    if os.path.exists(directorio_web):
        shutil.rmtree(directorio_web)
    
    os.mkdir(directorio_web)
    print ("creado")

    nombre_directorio_libros= "libros"
    directorio_libros = os.path.join(directorio_web, nombre_directorio_libros) #tengo la ruta absoluta del directorio donde irán copiados los libros
    if os.path.exists(directorio_libros):
        shutil.rmtree(directorio_libros)
    
    os.mkdir(directorio_libros)
    print ("creado")

    #Con estos pasos muevo los elementos estáticos del sitio a generar
    shutil.copytree (directorio_actual+"/estaticos", directorio_web+"/estaticos")         
    
    # Muevo el index_base a la carpeta sitio web
    shutil.copy ("index_base.html", directorio_web)  
    
    directorio_libros_a_copiar = filedialog.askdirectory()
    
    datos =""
    cantidad = 1

    print (directorio_libros)

    copiados = 0
    ignorados = 0 

    if "/" in directorio_libros_a_copiar and directorio_libros_a_copiar != directorio_libros: #quiere decir que seleccionó una carpeta :)

        pregunta = messagebox.askokcancel("askokcancel", "Se van a copiar los libros (PDF, EPUB, ODT o TXT). Continuá o cancelá...")
        
        if (pregunta == True):
            for ruta, carpeta, archivos in os.walk(directorio_libros_a_copiar):
                if ruta == directorio_libros:
                    messagebox.showwarning("showwarning", "¡La carpeta origen es igual a la destino! No se copiará.")
                    print (ruta+"<----origen es igual a -->"+directorio_libros)
                else:
                    for archivo in archivos:
                        if archivo.endswith (".pdf") or archivo.endswith (".epub") or archivo.endswith (".odt") or archivo.endswith (".txt"):
                            print ("Archivo cargado: " +archivo)
                            shutil.copy (ruta+"/"+archivo, directorio_libros)
                            copiados +=1
                        else:
                            print ("Archivo ignorado: " +archivo)
                            ignorados +=1
                    
            for ruta, carpeta, archivos in os.walk(directorio_libros):
                for archivo in archivos:
                    nom = archivo.split ('.')[0]
                    datos += """                     
                    <tr style="height: 40px; border-bottom: 1px solid rgba(226, 226, 226, 0.685);">
                        <th scope="row" style="font-size: 20px;">"""+str(cantidad)+"""</th>
                        <td><a href='"""+nombre_directorio_libros+"""/"""+archivo+"""' style="text-decoration: none; font-size: 20px; color: rgba(21, 96, 194, 0.904);" target='_blank'>"""+nom+"""</a></td>
                    </tr>
                    """
                    cantidad = cantidad +1

            #datos para el sitio
            titulo_web1 = 'sin titulo'
            descripcion_web1 = 'sin descripcion'
                  
            ws = Tk()
            ws.title("Datos para tu sitio web")
            ws.geometry('400x300')
            ws['bg'] = '#CCF0F5'

            def guardaDatos():
                titulo = titulo_web.get()
                titulo_web1 = titulo
                desc = descripcion.get()
                descripcion_web1 = desc
                ws.destroy()
                genera_sitio(directorio_web, titulo_web1, descripcion_web1, datos ) ## llama al modulo previamente importado
                
            Label(ws, text="Título del sitio web: ", background=subtitulo_fondo, fg=blanco,width=50, height=3).pack()
            titulo_web = Entry(ws)
            titulo_web.pack(pady=30)

            Label(ws, text="Breve descripción: ", background=subtitulo_fondo, fg=blanco,width=50, height=3).pack()              
            descripcion = Entry(ws)
            descripcion.pack(pady=30)

            Button(
                ws,
                text="Guardar info", 
                padx=10, 
                pady=5,
                command=guardaDatos
                ).pack()

            ws.mainloop()
                        
        else:
            print("Cancelado")
    elif "/" in directorio_libros_a_copiar and directorio_libros_a_copiar == directorio_libros:
        return messagebox.showwarning("showwarning", "¡La carpeta origen es igual a la destino!")
    else:
        return messagebox.showwarning("showwarning", "¡No se seleccionó ninguna carpeta!")
        
