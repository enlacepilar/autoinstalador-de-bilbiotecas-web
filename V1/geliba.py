from tkinter import *
import os
from tkinter import messagebox

#colores
letra_geliba="#1E434C"
#fondo ="#C99E10"
fondo = "#EBEBEB"
#subtitulo_fondo="#9B4F0F"
subtitulo_fondo = "#1F8A98"
boton_carpeta="#34C3D5"
letra_carpeta="black"
boton_ayuda="#78D7E3"
letra_ayuda="white"
blanco="white"



caja = Tk()

caja.title("Instalador")
caja.geometry('400x280')
caja.config(background=fondo)
caja.resizable(False, False)


texto0 = Label(caja, text="Ge.Li.B.A.", font=('Tahoma', 18), height=2, fg=letra_geliba, bg=fondo)
texto0.grid()

texto1 = Label(caja, text="¡Generador Libre de Bibliotecas Automatizadas \n (anti obsolescentes)!", background=subtitulo_fondo, fg=blanco,width=50, height=3)
texto1.grid()

texto2 = Label(caja, text="Hacer clic en el botón para generar el archivo", background=fondo, height=2)
texto2.grid()

### *** FUNCIÓN ABRE HTML  *** 
def mira_HTML ():
    import webbrowser
  
    # open html file
    #webbrowser.open('index.html') 
    filename = 'file:///'+os.getcwd()+'/' + 'index.html'
    webbrowser.open_new_tab(filename)

### *** FUNCIÓN ABRE HTML en GELIBA*** 
def mira_HTML_Externo (carpeta):
    import webbrowser
  
    # open html file
    #webbrowser.open('index.html') 
    filename = 'https://geliba.enlacepilar.com.ar/libros_externos/' + carpeta
    webbrowser.open_new_tab(filename)

### *** FUNCIÓN SUBE POR FTP A GELIBA *** 
def subir_ftp():
    print ("subiendo al FTP")
    from ftplib import FTP
    from tkinter import simpledialog


    nombre_carpeta_usuario = simpledialog.askstring("Input", "Escribí un nombre de usuario para la dirección web:")
    messagebox.showinfo('Listo para copiar', 'Presioná OK y aguardá el mensaje final de copia.')
    
    if nombre_carpeta_usuario is not None:
        print("Subiendo a: ", "https://geliba.enlacepilar.com.ar/libros_externos/"+nombre_carpeta_usuario)
    else:
        nombre_carpeta_usuario = "sin_nombre"
        
    ftp = FTP()
    ftp.connect ('geliba.enlacepilar.com.ar', 21)
    ftp.login()
    #print (ftp.getwelcome())

    # force UTF-8 encoding
    ftp.encoding = "utf-8"

    #Este directorio sirve para buscar lo local como para asignar lo remoto
    directorio_libros = ('libros')

    
    ftp.cwd("/")
    ftp.mkd(nombre_carpeta_usuario)
    ftp.cwd(nombre_carpeta_usuario)
    ftp.mkd(directorio_libros)
    
    
    #Copia los libros a la carpeta del usuario
    for ruta, carpeta, archivos in os.walk(directorio_libros):
        print (ruta)
        for archivo in archivos:
            if archivo.endswith (".pdf") or archivo.endswith (".epub") or archivo.endswith (".odt") or archivo.endswith (".txt"):
                print ("Archivo cargado: " +archivo)
                #subir_libros(archivo)
                fh = open(directorio_libros+"/"+archivo, 'rb')
                ftp.storbinary('STOR ' + directorio_libros+'/'+archivo, fh)

    #Copia el index.html previamente generado
    archivo_index = "index.html"
    fh = open( archivo_index, 'rb')
    ftp.storbinary('STOR ' + archivo_index, fh)

    messagebox.showinfo('¡Subida completa!', 'Archivo subido a https://geliba.enlacepilar.com.ar/libros_externos/'+nombre_carpeta_usuario+'. ¡Abriendo la página web de GELIBA con tus libros!.')
    ftp.close()
    mira_HTML_Externo (nombre_carpeta_usuario)


### *** FUNCIÓN GENERA LIBROS *** 
def generador_de_libros():
    
    import shutil
    from tkinter import filedialog

    directorio_actual = os.getcwd()
    nombre_carpeta = "libros"
    crear_carpeta = "/"+nombre_carpeta

    if (os.path.isdir(nombre_carpeta)):
        print ("La carpeta ya existe")
    else:
        os.mkdir(directorio_actual+crear_carpeta)
        
    directorio_nuevo = directorio_actual+crear_carpeta

    
    directorio_libros = filedialog.askdirectory()
    
    datos =""
    cantidad = 1

    print (directorio_libros)

    

    #print (directorio_libros)
    copiados = 0
    ignorados = 0 

    if "/" in directorio_libros and directorio_libros != directorio_nuevo: #quiere decir que seleccionó una carpeta :)

        #messagebox.showwarning("showwarning", "Se van a copiar los libros. Presione OK y aguarde...")
        pregunta = messagebox.askokcancel("askokcancel", "Se van a copiar los libros (PDF, EPUB, ODT o TXT). Continuá o cancelá...")
        print (pregunta)

        if (pregunta == True):
            for ruta, carpeta, archivos in os.walk(directorio_libros):        
                print (ruta)
                if ruta == directorio_nuevo:
                    messagebox.showwarning("showwarning", "¡La carpeta origen es igual a la destino! No se copiará.")
                    print (ruta+"<----origen es igual a -->"+directorio_nuevo)
                else:

                    for archivo in archivos:
                        if archivo.endswith (".pdf") or archivo.endswith (".epub") or archivo.endswith (".odt") or archivo.endswith (".txt"):
                            print ("Archivo cargado: " +archivo)
                            shutil.copy (ruta+"/"+archivo, directorio_nuevo)
                            copiados +=1
                        else:
                            print ("Archivo ignorado: " +archivo)
                            ignorados +=1
                    
            for ruta, carpeta, archivos in os.walk(directorio_nuevo):
                for archivo in archivos:
                    nom = archivo.split ('.')[0]
                    datos += """                     
                    <tr style="height: 40px; border-bottom: 1px solid rgba(226, 226, 226, 0.685);">
                        <th scope="row" style="font-size: 20px;">"""+str(cantidad)+"""</th>
                        <td><a href='"""+nombre_carpeta+"""/"""+archivo+"""' style="text-decoration: none; font-size: 20px; color: rgba(21, 96, 194, 0.904);" target='_blank'>"""+nom+"""</a></td>
                    </tr>
                    """
                    cantidad = cantidad +1



            def crear_web(tit, desc):
                inicio = open ('index.html', 'w')
            
                inicio.write (""" 
              <html lang="en">
                <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <title>Mi biblioteca Ge.Li.B.A.</title>
                </head>
                <body style="min-height: 100vh; background-color: rgba(235, 235, 235, 0.719); position: relative; padding-bottom: 80px;">
                    
                    <h1 style="color: rgb(255, 255, 255) ; background-color: rgba(21, 96, 194, 0.719); height: 150px; box-shadow: 2px 2px 15px 10px rgba(80, 80, 80, 0.459); display: flex; align-items: center; justify-content: center;">"""+tit+"""</h1>

                         <p style="margin: 50px; padding: 25px;">"""+desc+"""</p>

                    <div style="border: 1px solid rgba(21, 96, 194, 0.904); background-color: white; box-shadow: 2px 2px 15px 10px rgba(80, 80, 80, 0.459); width: 80%; margin: 90px auto 0 auto;">

                        <h2 style="text-align: center; color: black; height: 20px; padding-top: 30px;">Total de libros: """+str(cantidad-1)+""" </h2>
                       

                        <div style="width: 90px; margin: auto; border: 5px solid rgba(21, 96, 194, 0.904);"></div>

                        <h5 style="text-align: center; margin-top: 10px; color: rgba(21, 96, 194, 0.904);">Presioná <strong>CTRL</strong> + <strong>F</strong> para buscar</h2>

                        <table style="text-align: center; width: 100%; min-height: 250px;">
                            <thead style="height: 45px; border-bottom: 1px solid rgba(226, 226, 226, 0.685); background-color: rgba(21, 96, 194, 0.650);">
                            <tr>
                                <th scope="col" style="font-size: 20px;"># </th>
                                <th scope="col" style="font-size: 20px;">Nombre</th>
                            </tr>
                            </thead>
                            <tbody>
                                """+datos+"""
                            </tbody>
                        </table>

                    </div>

                    <div style="margin: 100px; color: saddlebrown;"> </div> 
                    <footer  style="box-shadow: 2px 2px 15px 10px rgba(80, 80, 80, 0.459); color: white; display: flex; justify-content: center; align-items: center; position: absolute; bottom: 0; width: 100%; padding: 40px; background-color: rgba(21, 96, 194, 0.719);"> Generado con <a href='http://geliba.enlacepilar.com.ar'> Ge.Li.B.A.</a> - licencia: <a href='https://www.spanish-translator-services.com/espanol/t/gnu/gpl-ar.html'>GPL</a> </footer>

                </body>
                </html>
            """)


                inicio.close()

                messagebox.showinfo("showinfo", "Libros copiados: "+str(copiados)+ ". Ignorados: "+str(ignorados)+ ".")
                respuesta=messagebox.askquestion("Subir a Geliba", "¿Querés subir los libros a GeLiBA?")
                if respuesta == 'yes' :
                    subir_ftp()
                else :
                    messagebox.showinfo('Volver', 'No subimos nada. Abriendo el navegador con el index.html local.')
                    mira_HTML()
                    #root.destroy()
                


            
            #datos para el sitio
            titulo_web1 = 'sin titulo'
            descripcion_web1 = 'sin descripcion'
            
                    
            ws = Tk()
            ws.title("Datos para tu sitio web")
            ws.geometry('400x300')
            ws['bg'] = '#CCF0F5'

            def guardaDatos():
                titulo = titulo_web.get()
                titulo_web1 = titulo
                desc = descripcion.get()
                descripcion_web1 = desc
                ws.destroy()
                crear_web(titulo, desc)
                
                #Label(ws, text=f'{titulo} titulo del sitio! - f{descripcion}' , pady=20, bg='#ffbf00').pack()

            Label(ws, text="Título del sitio web: ", background=subtitulo_fondo, fg=blanco,width=50, height=3).pack()
            titulo_web = Entry(ws)
            titulo_web.pack(pady=30)

            Label(ws, text="Breve descripción: ", background=subtitulo_fondo, fg=blanco,width=50, height=3).pack()              
            descripcion = Entry(ws)
            descripcion.pack(pady=30)

            Button(
                ws,
                text="Guardar info", 
                padx=10, 
                pady=5,
                command=guardaDatos
                ).pack()

            ws.mainloop()
            
            
        else:
            print("Cancelado")
    elif "/" in directorio_libros and directorio_libros == directorio_nuevo:
        messagebox.showwarning("showwarning", "¡La carpeta origen es igual a la destino!")
    else:
        messagebox.showwarning("showwarning", "¡No se seleccionó ninguna carpeta!")
        


### *** FUNCIÓN AYUDA *** 
def ayuda():
    archivo_de_texto = open ('ayuda.txt','r')
    mensaje = archivo_de_texto.read()
    
    ventana_ayuda = Toplevel() #Crea la caja donde va el texto
    ventana_ayuda.geometry('800x700')
    inserta = Label(ventana_ayuda,text=mensaje)
    inserta.pack()

boton_html= Button(caja, text="Seleccionar carpeta y generar HTML", bg=boton_carpeta, fg=letra_carpeta, height=2, command=generador_de_libros )
boton_html.grid(column=0, row=4)

boton_ayuda =Button(caja, text="Ayuda", fg =letra_ayuda, bg=boton_ayuda, command=ayuda)
boton_ayuda.grid(column=0, row=5)


caja.mainloop()



