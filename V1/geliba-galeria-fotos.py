#from tkinter import *
import os
from tkinter import messagebox, Tk, Label, Button, Entry
from PIL import Image
#import genera_galeria as GG


#colores
letra_geliba="#1E434C"
#fondo ="#C99E10"
fondo = "#EBEBEB"
#subtitulo_fondo="#9B4F0F"
subtitulo_fondo = "#1F8A98"
boton_carpeta="#34C3D5"
letra_carpeta="black"
boton_ayuda="#78D7E3"
letra_ayuda="white"
blanco="white"



caja = Tk()

caja.title("Instalador")
caja.geometry('400x280')
caja.config(background=fondo)
caja.resizable(False, False)


texto0 = Label(caja, text="Ge.Li.B.A.", font=('Tahoma', 18), height=2, fg=letra_geliba, bg=fondo)
texto0.grid()

texto1 = Label(caja, text="¡Generador Libre de Bibliotecas Automatizadas \n (versión galería de fotos)!", background=subtitulo_fondo, fg=blanco,width=50, height=3)
texto1.grid()

texto2 = Label(caja, text="Hacer clic en el botón para generar la galería", background=fondo, height=2)
texto2.grid()


### *** FUNCIÓN ABRE HTML  *** 
def mira_HTML ():
    import webbrowser
  
    # open html file
    #webbrowser.open('index.html') 
    filename = 'file:///'+os.getcwd()+'/' + 'index.html'
    webbrowser.open_new_tab(filename)



### *** FUNCIÓN GENERA GALERÍA DE IMÁGENES *** 
def generador_de_galeria():
    
    import shutil
    from tkinter import filedialog

    directorio_actual = os.getcwd()
    nombre_carpeta = "imagenes"
    crear_carpeta = "/"+nombre_carpeta

    if (os.path.isdir(nombre_carpeta)):
        print ("La carpeta ya existe")
    else:
        os.mkdir(directorio_actual+crear_carpeta)
        
    directorio_nuevo = directorio_actual+crear_carpeta

    
    directorio_fotos = filedialog.askdirectory()
    
    datos =""
    cantidad = 1

    print (directorio_fotos)

    

    #print (directorio_fotos)
    copiados = 0
    ignorados = 0 

    if "/" in directorio_fotos and directorio_fotos != directorio_nuevo: #quiere decir que seleccionó una carpeta :)

       
        pregunta = messagebox.askokcancel("askokcancel", "Se van a copiar los imágenes (JPEG, PNG o BMP). Continuá o cancelá...")
        print (pregunta)

        if (pregunta == True):
            for ruta, carpeta, archivos in os.walk(directorio_fotos):        
                print (ruta)
                if ruta == directorio_nuevo:
                    messagebox.showwarning("showwarning", "¡La carpeta origen es igual a la destino! No se copiará.")
                    print (ruta+"<----origen es igual a -->"+directorio_nuevo)
                else:

                    for archivo in archivos:
                        if archivo.endswith (".jpeg") or archivo.endswith (".jpg") or archivo.endswith (".png") or archivo.endswith (".bmp"):
                            print ("Archivo cargado: " +archivo)
                            shutil.copy (ruta+"/"+archivo, directorio_nuevo)
                            copiados +=1
                        else:
                            print ("Archivo ignorado: " +archivo)
                            ignorados +=1
                    
          
            fotos = ''
            fotos += '<tr style="height: 40px; border-bottom: 1px solid rgba(226, 226, 226, 0.685);">'
            n = 1

            for archivo in os.listdir(directorio_nuevo):
                if archivo.endswith('.jpg'):
                    #print (archivo)
                    if (n < 4):
                        im = Image.open(directorio_nuevo+'/'+archivo)
                        im.thumbnail((200,175), Image.ANTIALIAS)
                        nom = archivo.split ('.')[0]
                        tnom = "zz-mini_ "+ nom + ".jpg"
                        im.save(directorio_nuevo+"/"+tnom, "JPEG")
                        fotos+= '<td style ="border: 1px solid black; border-radius: 10px;"><a href="%s/%s" target="_blank"><img src="%s/%s"></a></td>' % (nombre_carpeta, archivo, nombre_carpeta, tnom)
                        n +=1
                        print (archivo)
                    else:
                        im = Image.open(directorio_nuevo+'/'+archivo)
                        im.thumbnail((200,175), Image.ANTIALIAS)
                        nom = archivo.split ('.')[0]
                        tnom = "zz-mini_ "+ nom + ".jpg"
                        im.save(directorio_nuevo+"/"+tnom, "JPEG")
                        fotos +='<td style ="border: 1px solid black; border-radius: 10px;"><a href="%s/%s" target="_blank"><img src="%s/%s"></a></td></tr>' % (nombre_carpeta, archivo, nombre_carpeta, tnom)
                        n = 1
                        print (archivo)
                elif archivo.endswith('.jpeg'):
                    #print (archivo)
                    if (n < 4):
                        im = Image.open(directorio_nuevo+'/'+archivo)
                        im.thumbnail((200,175), Image.ANTIALIAS)
                        nom = archivo.split ('.')[0]
                        tnom = "zz-mini_ "+ nom + ".jpg"
                        im.save(directorio_nuevo+"/"+tnom, "JPEG")
                        fotos+= '<td style ="border: 1px solid black; border-radius: 10px;"><a href="%s/%s" target="_blank"><img src="%s/%s"></a></td>' % (nombre_carpeta, archivo, nombre_carpeta, tnom)
                        n +=1
                        print (archivo)
                    else:
                        im = Image.open(directorio_nuevo+'/'+archivo)
                        im.thumbnail((200,175), Image.ANTIALIAS)
                        nom = archivo.split ('.')[0]
                        tnom = "zz-mini_ "+ nom + ".jpg"
                        im.save(directorio_nuevo+"/"+tnom, "JPEG")
                        fotos +='<td style ="border: 1px solid black; border-radius: 10px;"><a href="%s/%s" target="_blank"><img src="%s/%s"></a></td></tr>' % (nombre_carpeta, archivo, nombre_carpeta, tnom)
                        n = 1
                        print (archivo)

                elif archivo.endswith('.png'):
                    print (archivo)
                    if (n < 4):
                        im = Image.open(directorio_nuevo+'/'+archivo)
                        im.thumbnail((200,175), Image.ANTIALIAS)
                        nom = archivo.split ('.')[0]
                        tnom = "zz-mini_ "+ nom + ".png"
                        im.save(directorio_nuevo+"/"+tnom, "png")
                        fotos+= '<td style ="border: 1px solid black; border-radius: 10px;"><a href="%s/%s" target="_blank"><img src="%s/%s"></a></td>' % (nombre_carpeta, archivo, nombre_carpeta, tnom)
                        n +=1
                        print (archivo)
                    else:
                        im = Image.open(directorio_nuevo+'/'+archivo)
                        im.thumbnail((200,175), Image.ANTIALIAS)
                        nom = archivo.split ('.')[0]
                        tnom = "zz-mini_ "+ nom + ".png"
                        im.save(directorio_nuevo+"/"+tnom, "png")
                        fotos +='<td style ="border: 1px solid black; border-radius: 10px;"><a href="%s/%s" target="_blank"><img src="%s/%s"></a></td></tr>' % (nombre_carpeta, archivo, nombre_carpeta, tnom)
                        n = 1
                        print (archivo)
                elif archivo.endswith('.bmp'):
                    #print (archivo)
                    if (n < 4):
                        im = Image.open(directorio_nuevo+'/'+archivo)
                        im.thumbnail((200,175), Image.ANTIALIAS)
                        nom = archivo.split ('.')[0]
                        tnom = "zz-mini_ "+ nom + ".bmp"
                        im.save(directorio_nuevo+"/"+tnom, "bmp")
                        fotos+= '<td style ="border: 1px solid black; border-radius: 10px;"><a href="%s/%s" target="_blank"><img src="%s/%s"></a></td>' % (nombre_carpeta, archivo, nombre_carpeta, tnom)
                        n +=1
                        print (archivo)
                    else:
                        im = Image.open(directorio_nuevo+'/'+archivo)
                        im.thumbnail((200,175), Image.ANTIALIAS)
                        nom = archivo.split ('.')[0]
                        tnom = "zz-mini_ "+ nom + ".bmp"
                        im.save(directorio_nuevo+"/"+tnom, "bmp")
                        fotos +='<td style ="border: 1px solid black; border-radius: 10px;"><a href="%s/%s" target="_blank"><img src="%s/%s"></a></td></tr>' % (nombre_carpeta, archivo, nombre_carpeta, tnom)
                        n = 1
                        print (archivo)
                cantidad = cantidad +1
            fotos+= ('</tr></table>')
                








            def crear_web(tit, desc):
                inicio = open ('index.html', 'w')
            
                inicio.write (""" 
              <html lang="en">
                <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <title>Mi biblioteca Ge.Li.B.A. de imágenes</title>
                </head>
                <body style="min-height: 100vh; background-color: rgba(235, 235, 235, 0.719); position: relative; padding-bottom: 80px;">
                    
                    <h1 style="color: rgb(255, 255, 255) ; background-color: rgba(21, 96, 194, 0.719); height: 150px; box-shadow: 2px 2px 15px 10px rgba(80, 80, 80, 0.459); display: flex; align-items: center; justify-content: center;">"""+tit+"""</h1>

                         <p style="margin: 50px; padding: 25px;">"""+desc+"""</p>

                    <div style="border: 1px solid rgba(21, 96, 194, 0.904); background-color: white; box-shadow: 2px 2px 15px 10px rgba(80, 80, 80, 0.459); width: 80%; margin: 90px auto 0 auto;">

                        <h2 style="text-align: center; color: black; height: 20px; padding-top: 30px;">Total de imágenes: """+str(cantidad-1)+""" </h2>
                       

                        <div style="width: 90px; margin: auto; border: 5px solid rgba(21, 96, 194, 0.904);"></div>

                        <h5 style="text-align: center; margin-top: 10px; color: rgba(21, 96, 194, 0.904);">Presioná <strong>CTRL</strong> + <strong>F</strong> para buscar</h2>

                        <table style="text-align: center; width: 100%; min-height: 250px;">
                            <thead style="height: 45px; border-bottom: 1px solid rgba(226, 226, 226, 0.685); background-color: rgba(21, 96, 194, 0.650);">
                            
                            </thead>
                            <tbody>
                                """+fotos+"""
                            </tbody>
                        </table>

                    </div>

                    <div style="margin: 100px; color: saddlebrown;"> </div> 
                    <footer  style="box-shadow: 2px 2px 15px 10px rgba(80, 80, 80, 0.459); color: white; display: flex; justify-content: center; align-items: center; position: absolute; bottom: 0; width: 100%; padding: 40px; background-color: rgba(21, 96, 194, 0.719);"> Generado con <a href='http://geliba.enlacepilar.com.ar'> Ge.Li.B.A.</a> - licencia: <a href='https://www.spanish-translator-services.com/espanol/t/gnu/gpl-ar.html'>GPL</a> </footer>

                </body>
                </html>
            """)


                inicio.close()

                messagebox.showinfo("showinfo", "Imágenes copiadas: "+str(copiados)+ ". Ignoradas: "+str(ignorados)+ ".")

                mira_HTML()
                

            #datos para el sitio
            titulo_web1 = 'sin titulo'
            descripcion_web1 = 'sin descripcion'
            
                    
            ws = Tk()
            ws.title("Datos para tu sitio web")
            ws.geometry('400x300')
            ws['bg'] = '#CCF0F5'

            def guardaDatos():
                titulo = titulo_web.get()
                titulo_web1 = titulo
                desc = descripcion.get()
                descripcion_web1 = desc
                ws.destroy()
                crear_web(titulo, desc)
                
                #Label(ws, text=f'{titulo} titulo del sitio! - f{descripcion}' , pady=20, bg='#ffbf00').pack()

            Label(ws, text="Título del sitio web: ", background=subtitulo_fondo, fg=blanco,width=50, height=3).pack()
            titulo_web = Entry(ws)
            titulo_web.pack(pady=30)

            Label(ws, text="Breve descripción: ", background=subtitulo_fondo, fg=blanco,width=50, height=3).pack()              
            descripcion = Entry(ws)
            descripcion.pack(pady=30)

            Button(
                ws,
                text="Guardar info", 
                padx=10, 
                pady=5,
                command=guardaDatos
                ).pack()

            ws.mainloop()
            
            
        else:
            print("Cancelado")
    elif "/" in directorio_fotos and directorio_fotos == directorio_nuevo:
        messagebox.showwarning("showwarning", "¡La carpeta origen es igual a la destino!")
    else:
        messagebox.showwarning("showwarning", "¡No se seleccionó ninguna carpeta!")




### *** FUNCIÓN AYUDA *** 
def ayuda():
    import tkinter as tk
    archivo_de_texto = open ('ayuda-galeria.txt','r')
    mensaje = archivo_de_texto.read()
    
    ventana_ayuda = tk.Toplevel() #Crea la caja donde va el texto
    ventana_ayuda.geometry('800x700')
    inserta = Label(ventana_ayuda,text=mensaje)
    inserta.pack()

    

boton_html= Button(caja, text="Seleccionar carpeta y generar Galería", bg=boton_carpeta, fg=letra_carpeta, height=2, command=generador_de_galeria )
#boton_html= Button(caja, text="Seleccionar carpeta y generar HTML", bg=boton_carpeta, fg=letra_carpeta, height=2, command=GG.generador_de_galeria ) #si hago un modulo aparte como pensaba
boton_html.grid(column=0, row=4)

boton_ayuda =Button(caja, text="Ayuda", fg =letra_ayuda, bg=boton_ayuda, command=ayuda)
boton_ayuda.grid(column=0, row=5)


caja.mainloop()



